package com.rss.dao;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.github.sardine.DavResource;
import com.github.sardine.Sardine;
import com.github.sardine.SardineFactory;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

@Component
public class rssdao {
	@Autowired
	SardinConnection con;
	@Autowired
	 private Environment env;
	
	JSONObject reviewer=new JSONObject();	
	//Sardine sardine = SardineFactory.begin("dcisupportman", "Internal@123");
	
	//JSONObject rssdetails = new JSONObject();
	String U_email = "";
	DavResource U_url = null;
	URI root = null;

	public String readParentDir(String rootDirName, String documentToFind) {
		System.out.println("rootDirName = " + rootDirName);
		System.out.println("documentToFind = " + documentToFind);
		//System.out.println("sardin con = "+con.ConnectToSardin());
		String webdavPath = "";
		//Sardine sardine = SardineFactory.begin("dcisupportman", "Internal@123");
		Sardine sardine=con.ConnectToSardin();
	
		try {
			// GET LISTS
			List<DavResource> resources = sardine.list(rootDirName);
			System.out.println("resources sizee = " + resources.size());
			for (DavResource res : resources) {
				if (res.isDirectory() && (res.toString().replaceAll("0020", " ").contains(documentToFind))) {
					System.out.println("xxxxxx = "+res.toString().replaceAll("0020", " ").contains(documentToFind));
					String doctoDirname = res.toString();
					System.out.println("doctoDirname = " + doctoDirname);
					String result = doctoDirname.replaceAll("0020", " ");
					System.out.println("converted urlname:" + result);

					// read Users
					listUsers("https://review.docubuilder.com" + doctoDirname);
					webdavPath = "https://review.docubuilder.com" + doctoDirname;
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return webdavPath;
	}// end

	public JSONObject readWebDavDir(String dirName) {
		boolean flag=false;
		System.out.println("dirName in Readwebdir =  "+dirName);
		//Sardine sardine = SardineFactory.begin("dcisupportman", "Internal@123");
		Sardine sardine=con.ConnectToSardin();
		//System.out.println("readWebDavDir File name   = "+dirName.split("/")[3].replaceAll(".at.", "@").replaceAll(".xml", "") );
		JSONObject temp = new JSONObject();
		try {
			// GET LISTS
			List<DavResource> resources = sardine.list(dirName);
			System.out.println("resources  in readwebdavdir = "+resources.size());
			for (DavResource res : resources) {
				//System.out.println("res in readWebDavDir =  "+res.toString()); // calls the .toString() method.				
				Map<String, String> resMap = res.getCustomProps();

				// get meta of the file if any.
				for (Map.Entry<String, String> entry : resMap.entrySet()) {
					System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
				}
				System.out.println("check = "+res.toString().endsWith(".xml"));
				flag=res.toString().endsWith(".xml");
				System.out.println("flag = "+flag);
				
				if (flag) // check if its a XML Directory.
				{
				   	// READ XML and CONVERT TO JSON
					System.out.println("res in read  = "+res);
					String email=res.toString();
					String emailkey=email.split("/")[3].replaceAll(".at.", "@").replaceAll(".xml", "");
					
					System.out.println("emailkey = "+emailkey);
					InputStream is = sardine.get("https://review.docubuilder.com" + res.toString());
					
					String xmlString = IOUtils.toString(is, "utf-8");
					is.close();
					temp=convertToJSON(xmlString,emailkey);
				}
				else
				{
					System.out.println("in else");
					continue;					
					//return BlankReviewer;
				}
				
				// getCustomproperties(res);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(temp.isEmpty())
		{
			JSONObject NoReviewer=new JSONObject();
			temp.put("Noreviewer","No Reviewers");
			System.out.println("Empty object");
			//return temp;
		}
		
		return temp;
	}// end

	public void listUsers(String url) {
		//Sardine sardine = SardineFactory.begin("dcisupportman", "Internal@123");
		Sardine sardine=con.ConnectToSardin();
		
		try {
			if (sardine.exists(url)) {
				List<DavResource> resources = sardine.list(url);
				for (DavResource res : resources) {
					if (!res.isDirectory() && res.toString().endsWith(".xml")) {
						System.out.println("res in listuser = "+res.toString());
						System.out.println("User Email:"
								+ res.toString().split("/")[3].replaceAll(".at.", "@").replaceAll(".xml", ""));
						String uemail = res.toString().split("/")[3].replaceAll(".at.", "@").replaceAll(".xml", "");
						System.out.println("URL:" + res);
						
						U_email = uemail;
						U_url = res;
					}
				}
			}
			//System.out.println("%%%%%%%  listuser emailjson obj = "+emailjson.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}// end

	// to get any custom properties set to the file.
	public void getCustomproperties(DavResource res) {
		Map<String, String> customProps = res.getCustomProps();
		// Use custom properties...
		String author = (String) customProps.get("author");
		String title = (String) customProps.get("title");

		System.out.println("AUTHOR:" + author);
		System.out.println("TITLE:" + title);
	}// end

	public JSONObject convertToJSON(String xmlContent,String userEmail) {
				//emailjson.put(userEmail,userEmail);
				System.out.println("UserEmail in convert start = "+userEmail);
				JSONObject emailjson = new JSONObject();	
				String[] KeyArray = {"m:highlight", "m:text", "m:stamp", "m:link","m:freetext","m:caret"};
				try {
					int PRETTY_PRINT_INDENT_FACTOR = 4;
					JSONObject xmlJSONObj = XML.toJSONObject(xmlContent);
					JSONObject rssjson=(JSONObject) xmlJSONObj.get("rss");
					JSONObject channeljson=(JSONObject) rssjson.get("channel");
					if(channeljson.has("item"))
					{
						Object itemobj=channeljson.get("item");
						JSONArray Rssfeedarray=new JSONArray();
						
						if(itemobj instanceof JSONArray)
						{
							Rssfeedarray=(JSONArray) itemobj;
							
							JSONArray rssdetailsarray=new JSONArray();
							for(int i=0;i<Rssfeedarray.length();i++)
							{
								JSONObject rssdetails = new JSONObject();
								
								JSONObject itemjson=(JSONObject) Rssfeedarray.get(i);
								//System.out.println("Print obj from JSONArray = "+itemobj.toString());
								System.out.println("Comments from jsonarray = "+itemjson.get("description"));
								rssdetails.put("Comment",itemjson.get("description"));
								
								rssdetails.put("UserEmail",userEmail);
								
								for(int j=0;j<KeyArray.length;j++)
								{
									if(itemjson.has(KeyArray[j]))
									{
										
										//System.out.println(KeyArray[j]+" is true ");
										JSONObject keyjson=(JSONObject) itemjson.get(KeyArray[j]);
										rssdetails.put("creationdate",keyjson.get("creationdate"));
										rssdetails.put("type",keyjson.get("intent"));
										//System.out.println("creationdate = "+keyjson.get("creationdate"));	
										
									}		
									
								}
								rssdetailsarray.put(rssdetails);
								//emailjson.put(userEmail,rssdetails);
							}
							emailjson.put("rssdeatails",rssdetailsarray);
							reviewer.put(userEmail,emailjson);
					}	
						else
						{
							JSONObject rssdetails = new JSONObject();
							JSONObject itemjson=(JSONObject) itemobj;
							if(!itemjson.isEmpty())
							{
								JSONArray rssdetailsarray=new JSONArray();
								rssdetails.put("Comment",itemjson.get("description"));
								rssdetails.put("UserEmail",userEmail);
								for(int j=0;j<KeyArray.length;j++)
								{
									if(itemjson.has(KeyArray[j]))
									{						
										System.out.println(userEmail+" = "+KeyArray[j]+" is true ");
										JSONObject keyjson=(JSONObject) itemjson.get(KeyArray[j]);
										System.out.println("creationdate in else = "+keyjson.get("creationdate"));		
										rssdetails.put("CreationDate",keyjson.get("creationdate"));	
										rssdetails.put("type",keyjson.get("intent"));						
									}
								}
								rssdetailsarray.put(rssdetails);
								emailjson.put("rssdeatails",rssdetailsarray);
								reviewer.put(userEmail,emailjson);
							}// check not empty
							
											
						}	// end else
					}
					else
					{
						JSONObject rssdetails=new JSONObject();
				/*
				 * rssdetails.put("Comment",""); rssdetails.put("CreationDate","");
				 * rssdetails.put("UserEmail",""); rssdetails.put("type","");
				 */
						JSONArray rssdetailsarray= new JSONArray();
						//rssdetailsarray.put(rssdetails);
						emailjson.put("rssdeatails",rssdetailsarray);
						reviewer.put(userEmail,emailjson);
					}
					
					
					String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
					//System.out.println(jsonPrettyPrintString);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("Final reviewer obj = "+reviewer.toString());
				return reviewer;		
	}// end
	
	
	

}
